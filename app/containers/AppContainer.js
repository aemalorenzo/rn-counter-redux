import React, { Component } from 'react'
import ReactNative from 'react-native'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { ActionCreators } from '../actions'

const {
    View,
    Text,
    TouchableHighlight,
} = ReactNative

class AppContainer extends Component {
    addRecipe() {
        this.props.addRecipe();
    }

    render() {
        return <View>
            <Text style={{margin: 20, fontSize: 20}}>
                Recipe Count:
            </Text>
            <Text style={{margin: 20, fontSize: 45}}>
                {this.props.recipeCount}
            </Text>
            <TouchableHighlight style={{padding: 16, backgroundColor: '#CCC'}} onPress={() => {this.addRecipe() }}>
                <Text style={{color: '#FFF'}}>Add recipe</Text>
            </TouchableHighlight>
        </View>
    }
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators(ActionCreators, dispatch);
}

export default connect((state) => { 
    return {
        recipeCount: state.recipeCount
    }
 }, mapDispatchToProps)(AppContainer);